<html>
<head>
    <title>Blogging system </title>
    <link rel="stylesheet" href="">
    <style>
body
{
    background-color:#efefef !important;
}
        .single-blog
        {
            box-shadow : 0px 0px 20px 1px(0,0,0,0.2);
            padding : 10px;
            margin-top : 30px;
            background-color: #fff;
        }

        .single-blog img
        {
            width: 100%;
        }

        .blog-meta
        {
            font-size : 14px;
            margin-bottom :2px;
        }

        .single-blog span
        {
            float: right;
            font-size :12px;
            color : cornflowerblue;
        }

        .blog-text
        {
            font-size : 14px;
            text-align :justify;
        }

        .single-blog h2
        {
            margin-top: 10px;
            font-size: 16px;
            color: #007bff;
        }

        .single-blog h2 a
        {
            text-decoration: none;
        }

        .read-more-btn
        {

            padding: 5px 12px 8px;
            border-radius: 20px;
            line-height: 20px;
            font-size:14px;
            display: inline-block;
            cursor: pointer;
            text-align: center;
            text-decoration: none;
            outline: none;
            color: #fff;
            background-color: #4CAF50;
            border: none;
            box-shadow: 0 9px #999;


        }

        .read-more-btn:hover
        {
            background-color: #3e8e41;
            text-decoration: none;
        }

        .read-more-btn:active{
            background-color: #3e8e41;
            box-shadow: 0 5px #666;
            transform: translateY(4px);

        }


/* Full-width input fields */
input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

/* Set a style for all buttons */
button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

button:hover {
    opacity: 0.8;
}

/* Extra styles for the cancel button */
.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}


.contain {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    padding-top: 40px;
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
    border: 1px solid #888;
    width: 40%; /* Could be more or less, depending on screen size */
}

/* The Close Button (x) */
.close {
    position: absolute;
    right: 25px;
    top: 0;
    color: #000;
    font-size: 35px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: red;
    cursor: pointer;
}

/* Add Zoom Animation */
.animate {
    -webkit-animation: animatezoom 0.6s;
    animation: animatezoom 0.6s
}

@-webkit-keyframes animatezoom {
    from {-webkit-transform: scale(0)}
    to {-webkit-transform: scale(1)}
}

@keyframes animatezoom {
    from {transform: scale(0)}
    to {transform: scale(1)}
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
        display: block;
        float: none;
    }
    .cancelbtn {
        width: 100px;
    }
}



    </style>
</head>

<body>
 <div class="container">
     <h1 class ="text-center">Blog</h1>
        <p class ="text-center">.....</p>
     <div class ="row">
         <div class="col-md-4">
             <div class ="single-blog">
                 <p class ="blog-meta">By Admin <span> Apr 2,2019</span></p>
                 <img src="/yii2/yii-application/images/1.jpg">
                 <h2><a href="#">iPhone get a price drop in China</a> </h2>
                 <p class="blog-text">Apple  this week lowered the price on a number of key hardware lines in China, including AirPods, Macs, iPads and, most notably, the iPhone. The move, noted by CNBC, is believed to be the direct result of a 3 percent tax cut that took effect in the country yesterday.

                     In many cases, however, the impacted products have dropped by even more, including a 500 yuan ($74) price cut to the iPhone XS, marking a nearly 6 percent drop for the company’s latest flagship.

                  </p>

                 <button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Comment</button>




                 <div id="id01" class="modal">

                     <form class="modal-content animate" action="">
                         <div class="imgcontainer">
                             <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>

                         </div>

                         <div class="contain">
                             <input type="text" placeholder="Enter Name" name="name" required>


                             <textarea rows="10" cols="100" name="comment" required>Comment here...</textarea>

                             <button type="submit">Submit</button>

                         </div>

                         <div class="contain" style="background-color:#f1f1f1">
                             <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>

                         </div>
                     </form>
                 </div>

                 <script>
                     // Get the modal
                     var modal = document.getElementById('id01');

                     // When the user clicks anywhere outside of the modal, close it
                     window.onclick = function(event) {
                         if (event.target == modal) {
                             modal.style.display = "none";
                         }
                     }
                 </script>




             </div>
         </div>

         <div class ="col-md-4">
             <div class ="single-blog">
                 <p class ="blog-meta">By Admin <span> Apr 2,2019</span></p>
                 <img src="/yii2/yii-application/images/1.jpg">
                 <h2><a href="#">iPhone get a price drop in China</a> </h2>
                 <p class="blog-text">Apple  this week lowered the price on a number of key hardware lines in China, including AirPods, Macs, iPads and, most notably, the iPhone. The move, noted by CNBC, is believed to be the direct result of a 3 percent tax cut that took effect in the country yesterday.

                     In many cases, however, the impacted products have dropped by even more, including a 500 yuan ($74) price cut to the iPhone XS, marking a nearly 6 percent drop for the company’s latest flagship.

                 </p>


                 <button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Comment</button>

             </div>
         </div>

         <div class ="col-md-4">

         <div class ="single-blog">
             <p class ="blog-meta">By Admin <span> Apr 2,2019</span></p>
             <img src="/yii2/yii-application/images/1.jpg">
             <h2><a href="#">iPhone get a price drop in China</a> </h2>
             <p class="blog-text">Apple  this week lowered the price on a number of key hardware lines in China, including AirPods, Macs, iPads and, most notably, the iPhone. The move, noted by CNBC, is believed to be the direct result of a 3 percent tax cut that took effect in the country yesterday.

                 In many cases, however, the impacted products have dropped by even more, including a 500 yuan ($74) price cut to the iPhone XS, marking a nearly 6 percent drop for the company’s latest flagship.

             </p>


             <button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Comment</button>
         </div>
     </div>
         </div>

     <div class ="row">
         <div class="col-md-4">
             <div class ="single-blog">
                 <p class ="blog-meta">By Admin <span> Apr 2,2019</span></p>
                 <img src="/yii2/yii-application/images/1.jpg">
                 <h2><a href="#">iPhone get a price drop in China</a> </h2>
                 <p class="blog-text">Apple  this week lowered the price on a number of key hardware lines in China, including AirPods, Macs, iPads and, most notably, the iPhone. The move, noted by CNBC, is believed to be the direct result of a 3 percent tax cut that took effect in the country yesterday.

                     In many cases, however, the impacted products have dropped by even more, including a 500 yuan ($74) price cut to the iPhone XS, marking a nearly 6 percent drop for the company’s latest flagship.

                 </p>


                 <button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Comment</button>
             </div>
         </div>

         <div class ="col-md-4">
             <div class ="single-blog">
                 <p class ="blog-meta">By Admin <span> Apr 2,2019</span></p>
                 <img src="/yii2/yii-application/images/1.jpg">
                 <h2><a href="#">iPhone get a price drop in China</a> </h2>
                 <p class="blog-text">Apple  this week lowered the price on a number of key hardware lines in China, including AirPods, Macs, iPads and, most notably, the iPhone. The move, noted by CNBC, is believed to be the direct result of a 3 percent tax cut that took effect in the country yesterday.

                     In many cases, however, the impacted products have dropped by even more, including a 500 yuan ($74) price cut to the iPhone XS, marking a nearly 6 percent drop for the company’s latest flagship.

                 </p>


                 <button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Comment</button>
             </div>
         </div>

         <div class ="col-md-4">

             <div class ="single-blog">
                 <p class ="blog-meta">By Admin <span> Apr 2,2019</span></p>
                 <img src="/yii2/yii-application/images/1.jpg">
                 <h2><a href="#">iPhone get a price drop in China</a> </h2>
                 <p class="blog-text">Apple  this week lowered the price on a number of key hardware lines in China, including AirPods, Macs, iPads and, most notably, the iPhone. The move, noted by CNBC, is believed to be the direct result of a 3 percent tax cut that took effect in the country yesterday.

                     In many cases, however, the impacted products have dropped by even more, including a 500 yuan ($74) price cut to the iPhone XS, marking a nearly 6 percent drop for the company’s latest flagship.

                 </p>

                 <button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Comment</button>

             </div>
         </div>
     </div>


 </div>

     </div>

 </div>
</body>
</html>